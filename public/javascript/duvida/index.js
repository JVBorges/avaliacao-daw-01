$(() => {
  const toast = new bootstrap.Toast(document.getElementById("toast"));

  enviar = () => {
    const email = $('#inputEmail').val();
    const mensagem = $('#inputMensagem').val();
    const url = new URL('http://localhost:3000/api/mensagem');

    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email, mensagem })
    }).then(res => res.json())
      .then(data => {
        $('.toast-body').empty();
        $('#toast').removeClass('bg-danger');
        $('#toast').removeClass('bg-success');
        $('#toast').addClass(() => `bg-${data.type}`);
        $('.toast-body').append(data.message);
        toast.show();
      })
      .catch(err => {
        console.error(err);
      });
  }

  $("form").submit(e => {
    e.preventDefault();
    enviar();
  });
});