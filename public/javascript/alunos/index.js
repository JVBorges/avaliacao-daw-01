$(() => {
  const toast = new bootstrap.Toast(document.getElementById("toastError"));

  getAlunos = async (filtro) => {
    let url = new URL('http://localhost:3000/api/alunos'),
      params = { f: filtro };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

    try {
      const response = await fetch(url);
      if (!response.ok) throw await response.json();
      const alunos = await response.json();
      return alunos;
    } catch (err) {
      $('.toast-body').append(err.message);
      console.error(new Error(err.message));
      toast.show();
    }
  };

  preencheTable = async (value = '') => {
    const alunos = await getAlunos(value);
    const tableBody = $('tbody').empty();

    if (Array.isArray(alunos) && alunos.length) {
      alunos.forEach(aluno => {
        const row = `<tr><td>${aluno.nome}</td><td>${aluno.email}</td></tr>`;
        tableBody.append(row);
      });
    } else {
      tableBody.append('<tr><td align="center" colspan="2">Sem registros!</td></tr>');
    }

  }

  preencheTable();

  filtrar = () => {
    const input = $('#filterInput').val();
    preencheTable(input);
  }

  $("form").submit(e => {
    e.preventDefault();
    filtrar();
  });
});