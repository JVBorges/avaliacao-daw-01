const fs = require('fs');
const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(`${__dirname}/public`));

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/public/html/index.html`);
});

app.get('/eu', (req, res) => {
  res.sendFile(`${__dirname}/public/html/eu.html`);
});

app.get('/alunos', (req, res) => {
  res.sendFile(`${__dirname}/public/html/aluno.html`);
});

app.get('/duvida', (req, res) => {
  res.sendFile(`${__dirname}/public/html/duvida.html`);
});

app.get('/api/alunos', (req, res) => {
  const { f } = req.query;

  fs.readFile(`${__dirname}/alunos.json`, 'utf-8', (err, data) => {
    if (err)
      return res.status(500).json({ message: 'Erro ao ler arquivo' });

    let alunos = JSON.parse(data);

    if (f)
      alunos = alunos.filter(aluno => aluno.nome.toString().toUpperCase().includes(f.toUpperCase()));

    res.json(alunos);
  });
});

app.post('/api/mensagem', (req, res) => {
  const { email, mensagem } = req.body;

  if (email && mensagem) {
    res.status(201).json({ type: 'success', message: `A dúvida: ${mensagem.length > 35 ? `${mensagem.substring(0, 35)}...` : mensagem} foi enviada com sucesso e será respondida no e-mail: ${email}` });
  } else {
    res.status(400).json({ type: 'danger', message: 'Por favor verificar se todos os campos foram preenchidos!' });
  }
});

app.listen(3000, () => console.log('Serve running at port 3000'));